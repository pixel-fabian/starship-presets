# Starship presets

Presets for [starship](https://starship.rs) cross-shell prompt.

1. Have a [Nerd Font](https://www.nerdfonts.com) installed and enabled in your shell. This makes sure the icons from `Symbols Nerd Font` are available.
2. Have staship installed and activated in your shell
3. Update staship config: `# ~/.config/starship.toml`

## Presets

### Bielefeld Night

Inspired by [Tokyo Night](https://starship.rs/presets/tokyo-night.html).

#### Examples:

- Bielefeld Night on Windows VSCode git bash \
![Bielefeld Night on Windows VSCode git bash](./bielefeld-night/bielefeld-night-03.jpg)

- Bielefeld Night on Ubuntu Fish \
![Bielefeld Night on Ubuntu Fish](./bielefeld-night/bielefeld-night-02.jpg)

- Bielefeld Night on Windows PowerShell as admin \
![Bielefeld Night on Windows PowerShell as admin](./bielefeld-night/bielefeld-night-01.jpg)


## Possible issues

### Icons overlap each other

- Depeding on the chosen font, the width of unicode characters might be different. I recommend monospace fonts like [FiraMono Nerd Font](https://www.nerdfonts.com/font-downloads).
- Rendering of Unicode 9 characters can be changed in some terminals. See [spaceship FAQ](https://spaceship-prompt.sh/faq/#Some-section-icons-overlap-each-other).